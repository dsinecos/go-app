FROM golang:1.16-alpine AS go-build

WORKDIR /api

COPY go.mod .
COPY go.sum .
COPY vendor/ vendor

COPY cmd/ cmd
COPY pkg/ pkg

RUN go build -o go-app -mod=vendor cmd/api-server/*.go

FROM alpine:3.14

COPY --from=go-build /api/go-app .

EXPOSE 9010

CMD ./go-app
