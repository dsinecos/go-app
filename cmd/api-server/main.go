package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/dsinecos/go-app/pkg/app"
	"github.com/dsinecos/go-app/pkg/router"
)

func main() {
	deps := app.NewDependencies()

	r := router.New(deps)

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", strconv.Itoa(deps.Config.Port)),
		Handler: r,
	}

	go func(server *http.Server) {
		deps.Logger.Infof("Starting server on 0.0.0.0:%s", strconv.Itoa(deps.Config.Port))
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			deps.Logger.Fatalf("error starting server %s", err)
		}
	}(server)

	shutdownCh := make(chan os.Signal, 2)
	signal.Notify(shutdownCh, syscall.SIGINT, syscall.SIGTERM)
	<-shutdownCh

	// Flush logs
	deps.Logger.Sync()

	err := server.Shutdown(context.Background())
	if err != nil {
		deps.Logger.Fatalf("error shutting down server %s", err)
	}
}
