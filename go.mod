module github.com/dsinecos/go-app

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.4
	github.com/heetch/confita v0.10.0
	github.com/stretchr/testify v1.7.0
	go.elastic.co/ecszap v1.0.0
	go.mongodb.org/mongo-driver v1.7.2
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.19.1
)
