package config

import (
	"context"

	"github.com/dsinecos/go-app/pkg/logger"
	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/env"
)

const (
	defaultPort     = 9010
	defaultLogLevel = logger.LogLevelDebug
)

type Config struct {
	Port                    int    `config:"PORT"`
	MongoScheme             string `config:"MONGO_SCHEME,required"`
	MongoUser               string `config:"MONGO_USER,required"`
	MongoPassword           string `config:"MONGO_PASSWORD,required"`
	MongoHost               string `config:"MONGO_HOST,required"`
	MongoConnectionSettings string `config:"MONGO_CONNECTION_SETTINGS"`
	LogLevel                string `config:"LOG_LEVEL"`
}

func New() (*Config, error) {
	c := Config{
		Port: defaultPort,
	}

	err := c.loadEnv(context.Background())
	if err != nil {
		return nil, err
	}

	return &c, nil
}

func (c *Config) loadEnv(ctx context.Context) error {
	loader := confita.NewLoader(
		env.NewBackend(),
	)

	return loader.Load(ctx, c)
}
