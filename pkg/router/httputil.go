package router

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

func WriteKeyValueResponse(httpStatusCode int, key, value string, w http.ResponseWriter) error {

	resp := keyValueResponse{
		Code:    strconv.Itoa(httpStatusCode),
		Message: messageSuccess,
		Key:     key,
		Value:   value,
	}

	serialResp, err := json.Marshal(resp)
	if err != nil {
		return fmt.Errorf("error marshalling response to json %w", err)
	}

	w.WriteHeader(httpStatusCode)
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(serialResp)
	if err != nil {
		return fmt.Errorf("error writing response %w", err)
	}
	return nil
}

func WriteRecordResponse(httpStatusCode int, r []aggregateRecord, w http.ResponseWriter) error {

	resp := getRecordResponse{
		Code:    httpStatusCode,
		Message: messageSuccess,
		Records: r,
	}

	serialResp, err := json.Marshal(resp)
	if err != nil {
		return fmt.Errorf("error marshalling response to json %w", err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatusCode)
	_, err = w.Write(serialResp)
	if err != nil {
		return fmt.Errorf("error writing response %w", err)
	}
	return nil
}

func WriteErrorResponse(err error, w http.ResponseWriter) error {

	respHTTPStatusCode := ErrortoHTTPStatusCode(err)

	resp := errorResponse{
		Code:    respHTTPStatusCode,
		Message: ToAPIErrorMessage(err),
	}

	serialResp, err := json.Marshal(resp)
	if err != nil {
		return fmt.Errorf("error marshalling response to json %w", err)
	}

	w.WriteHeader(respHTTPStatusCode)
	_, err = w.Write(serialResp)
	if err != nil {
		return fmt.Errorf("error writing response %w", err)
	}
	return nil
}
