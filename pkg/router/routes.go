package router

import (
	"net/http"

	"github.com/dsinecos/go-app/pkg/app"
	"github.com/go-chi/chi/v5"
)

func New(deps *app.Dependencies) http.Handler {

	// Initialize Handlers
	k := keyValueHandler{
		s:      deps.KeyValueService,
		logger: deps.Logger,
	}

	r := recordHandler{
		s:      deps.RecordService,
		logger: deps.Logger,
	}

	mux := chi.NewRouter()
	mux.Get("/records", r.get)
	mux.Post("/in-memory", k.put)
	mux.Get("/in-memory", k.get)

	return mux
}
