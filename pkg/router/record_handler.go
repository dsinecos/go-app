package router

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/dsinecos/go-app/pkg/domain"
	"github.com/dsinecos/go-app/pkg/logger"
)

const (
	dateLayout = "2006-01-02"
)

type recordHandler struct {
	s      domain.RecordService
	logger *logger.Logger
}

type Date struct {
	time.Time
}

func (d *Date) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return fmt.Errorf("error unmarshalling date %w", err)
	}

	t, err := time.Parse(dateLayout, s)
	if err != nil {
		return fmt.Errorf("error parsing date %w", err)
	}

	d.Time = t
	return nil
}

func (d Date) MarshalJSON() ([]byte, error) {
	// TODO - Review
	return json.Marshal(d.Time.Format(dateLayout))
}

type inputParams struct {
	StartDate Date `json:"startDate"`
	EndDate   Date `json:"endDate"`
	MinCount  int  `json:"minCount"`
	MaxCount  int  `json:"maxCount"`
}

type aggregateRecord struct {
	Key        string    `json:"key"`
	CreatedAt  time.Time `json:"createdAt"`
	TotalCount int       `json:"totalCount"`
}

type getRecordResponse struct {
	Code    int               `json:"code"`
	Message string            `json:"msg"`
	Records []aggregateRecord `json:"records"`
}

func (h *recordHandler) get(w http.ResponseWriter, r *http.Request) {

	var i inputParams

	err := json.NewDecoder(r.Body).Decode(&i)
	if err != nil {
		h.logger.Errorf("error unmarshalling request body %s", err)

		err = WriteErrorResponse(err, w)
		if err != nil {
			h.logger.Errorf("error writing response %s", err)
		}
		return
	}
	defer r.Body.Close()

	aggregateRecords, err := h.s.GetRecordsBetweenDateAndCount(r.Context(), domain.GetRecordsInputParams{
		StartDate: i.StartDate.Time,
		EndDate:   i.EndDate.Time,
		MinCount:  i.MinCount,
		MaxCount:  i.MaxCount,
	})
	if err != nil {
		h.logger.Errorf("error getting records from Records service -> %s", err)

		err = WriteErrorResponse(err, w)
		if err != nil {
			h.logger.Errorf("error writing response %s", err)
		}
		return
	}

	respRecords := convertToResponseAggregateRecords(aggregateRecords)

	err = WriteRecordResponse(http.StatusOK, respRecords, w)
	if err != nil {
		h.logger.Errorf("error writing response %s", err)
		return
	}
}

func convertToResponseAggregateRecords(a []domain.AggregateRecord) []aggregateRecord {
	var result []aggregateRecord

	for _, r := range a {
		result = append(result, aggregateRecord{
			Key:        r.Key,
			CreatedAt:  r.CreatedAt,
			TotalCount: r.TotalCount,
		})
	}

	return result
}
