package router

import (
	"errors"
	"net/http"

	"github.com/dsinecos/go-app/pkg/domain"
)

const (
	messageSuccess string = "Success"
)

type errorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"msg"`
}

func ErrortoHTTPStatusCode(err error) int {

	var domainErr *domain.Error

	if errors.As(err, &domainErr) {
		switch domainErr.Code {
		case domain.EINVALID:
			return http.StatusBadRequest
		case domain.EINTERNAL:
			return http.StatusInternalServerError
		case domain.ENOTFOUND:
			return http.StatusNotFound
		}
	}

	return http.StatusInternalServerError
}

func ToAPIErrorMessage(err error) string {
	var domainErr *domain.Error

	if errors.As(err, &domainErr) && domainErr.Message != "" {
		return domainErr.Message
	}

	return "Error processing request. Contact support for help"
}
