package router

import (
	"encoding/json"
	"net/http"

	"github.com/dsinecos/go-app/pkg/domain"
	"github.com/dsinecos/go-app/pkg/logger"
)

const (
	queryKey = "key"
)

type inputCell struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type keyValueResponse struct {
	Code    string `json:"code"`
	Message string `json:"msg"`
	Key     string `json:"key"`
	Value   string `json:"value"`
}

type keyValueHandler struct {
	s      domain.KeyValueService
	logger *logger.Logger
}

func (k *keyValueHandler) put(w http.ResponseWriter, r *http.Request) {
	var c inputCell

	err := json.NewDecoder(r.Body).Decode(&c)
	if err != nil {
		k.logger.Errorf("error unmarshalling request body %s", err)

		err = WriteErrorResponse(err, w)
		if err != nil {
			k.logger.Errorf("error writing response %s", err)
		}
		return
	}
	defer r.Body.Close()

	cell, err := k.s.Put(r.Context(), c.Key, c.Value)
	if err != nil {
		k.logger.Errorf("error returned from KeyValue service -> %s", err)

		err = WriteErrorResponse(err, w)
		if err != nil {
			k.logger.Errorf("error writing response %s", err)
		}
		return
	}

	err = WriteKeyValueResponse(http.StatusCreated, cell.Key, cell.Value, w)
	if err != nil {
		k.logger.Errorf("error writing response %s", err)
		return
	}
}

func (k *keyValueHandler) get(w http.ResponseWriter, r *http.Request) {
	key := r.URL.Query().Get(queryKey)

	cell, err := k.s.Get(r.Context(), key)
	if err != nil {
		k.logger.Errorf("error returned from KeyValue service -> %s", err)

		err = WriteErrorResponse(err, w)
		if err != nil {
			k.logger.Errorf("error writing response %s", err)
		}
		return
	}

	err = WriteKeyValueResponse(http.StatusOK, cell.Key, cell.Value, w)
	if err != nil {
		k.logger.Errorf("error writing response %s", err)
		return
	}
}
