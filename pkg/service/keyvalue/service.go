package keyvalue

import (
	"context"

	"github.com/dsinecos/go-app/pkg/domain"
	"github.com/dsinecos/go-app/pkg/logger"
)

const (
	serviceName string = "KeyValueService"
	putMethod   string = "Put"
	getMethod   string = "Get"
)

type InMemoryRepo interface {
	Put(ctx context.Context, key, value string) domain.Cell
	Get(ctx context.Context, key string) (domain.Cell, bool)
}

type Service struct {
	InMemoryRepo InMemoryRepo
	Logger       *logger.Logger
}

func (s *Service) Put(ctx context.Context, key, value string) (domain.Cell, error) {
	const Op = serviceName + ":" + putMethod

	if isEmpty(key) || isEmpty(value) {
		// TODO - Update error
		return domain.Cell{}, &domain.Error{Code: domain.EINVALID, Message: "Key and Value should be non-empty", Op: Op}
	}

	c := s.InMemoryRepo.Put(ctx, key, value)

	return c, nil
}

func (s *Service) Get(ctx context.Context, key string) (domain.Cell, error) {
	const Op = serviceName + ":" + getMethod

	if isEmpty(key) {
		// TODO - Update error
		return domain.Cell{}, &domain.Error{Code: domain.EINVALID, Message: "Key should be non-empty", Op: Op}
	}

	c, ok := s.InMemoryRepo.Get(ctx, key)
	if !ok {
		// TODO - Update error
		return domain.Cell{}, &domain.Error{Code: domain.ENOTFOUND, Message: "Key not found", Op: Op}
	}

	return c, nil
}

func isEmpty(s string) bool {
	return s == ""
}
