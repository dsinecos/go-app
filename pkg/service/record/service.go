package record

import (
	"context"
	"time"

	"github.com/dsinecos/go-app/pkg/domain"
	"github.com/dsinecos/go-app/pkg/logger"
)

const (
	serviceName                   string = "RecordsService"
	getRecordsBetweenDateAndCount string = "GetRecordsBetweenDateAndCount"
)

type RecordsRepo interface {
	GetRecordsBetweenDateAndCount(ctx context.Context, startDate, endDate time.Time, minCount, maxCount int) ([]domain.AggregateRecord, error)
}

type Service struct {
	RecordsRepo RecordsRepo
	Logger      *logger.Logger
}

func (s *Service) GetRecordsBetweenDateAndCount(ctx context.Context, i domain.GetRecordsInputParams) ([]domain.AggregateRecord, error) {
	const Op = serviceName + ":" + getRecordsBetweenDateAndCount

	if isInvalid(i) {
		return nil, &domain.Error{Code: domain.EINVALID, Message: "StartDate, EndDate should be non-empty", Op: Op}
	}

	records, err := s.RecordsRepo.GetRecordsBetweenDateAndCount(ctx, i.StartDate, i.EndDate, i.MinCount, i.MaxCount)
	if err != nil {
		// TODO - Update error
		return nil, &domain.Error{Code: domain.EINTERNAL, Message: "Internal Server Error. Contact support for help", Op: Op, Err: err}
	}

	return records, nil
}

func isInvalid(i domain.GetRecordsInputParams) bool {
	return i.StartDate.IsZero() || i.EndDate.IsZero()
}
