package domain

import (
	"bytes"
	"fmt"
)

type ErrorCode string

const (
	EINTERNAL ErrorCode = "INTERNAL"
	EINVALID  ErrorCode = "INVALID"
	ENOTFOUND ErrorCode = "NOT_FOUND"
)

type Error struct {
	// Machine readable error code
	Code ErrorCode

	// Human readable message
	Message string

	// Logical operation and nested error
	Op  string
	Err error
}

func (err *Error) Error() string {
	// TODO
	var buf bytes.Buffer

	if err.Op != "" {
		fmt.Fprintf(&buf, "%s: ", err.Op)
	}

	if err.Err != nil {
		buf.WriteString(err.Err.Error())
	} else {
		if err.Code != "" {
			fmt.Fprintf(&buf, "<%s> ", err.Code)
		}
		buf.WriteString(err.Message)
	}
	return buf.String()
}

func (err *Error) Unwrap() error {
	return err.Err
}
