package domain

import "context"

type Cell struct {
	Key   string
	Value string
}

type KeyValueService interface {
	Put(ctx context.Context, key, value string) (Cell, error)
	Get(ctx context.Context, key string) (Cell, error)
}
