package domain

import (
	"context"
	"time"
)

// TODO - Remove JSON tags at domain level
type AggregateRecord struct {
	Key        string
	CreatedAt  time.Time
	TotalCount int
}

type GetRecordsInputParams struct {
	StartDate time.Time
	EndDate   time.Time
	MinCount  int
	MaxCount  int
}

type RecordService interface {
	GetRecordsBetweenDateAndCount(ctx context.Context, i GetRecordsInputParams) ([]AggregateRecord, error)
}
