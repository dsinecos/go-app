package app

import (
	"log"

	"github.com/dsinecos/go-app/pkg/config"
	"github.com/dsinecos/go-app/pkg/domain"
	"github.com/dsinecos/go-app/pkg/logger"
	"github.com/dsinecos/go-app/pkg/service/keyvalue"
	"github.com/dsinecos/go-app/pkg/service/record"
	"github.com/dsinecos/go-app/pkg/storage/db"
	"github.com/dsinecos/go-app/pkg/storage/inmemory"

	"go.mongodb.org/mongo-driver/mongo"
)

const (
	MongoDatabaseName     = "getir-case-study"
	RecordsCollectionName = "records"
)

type Dependencies struct {
	Config *config.Config
	Logger *logger.Logger

	MongoClient *mongo.Client

	KeyValueService domain.KeyValueService
	RecordService   domain.RecordService
}

func NewDependencies() *Dependencies {

	c, err := config.New()
	if err != nil {
		log.Fatalf("Error loading configuration %s", err)
	}

	logger, err := logger.New(logger.LogLevel(c.LogLevel))
	if err != nil {
		log.Fatalf("error building logger %s", err)
	}

	inMemoryStore := inmemory.NewStore()

	kvs := &keyvalue.Service{
		// InMemoryRepo: inmemory.NewInMemoryRepo(inMemoryStore, logger),
		InMemoryRepo: &inmemory.InMemoryRepo{
			Store:  inMemoryStore,
			Logger: logger,
		},
		Logger: logger,
	}

	mongoClient, err := db.GetMongoClient(db.MongoClientConfig{
		MongoScheme:             c.MongoScheme,
		MongoUser:               c.MongoUser,
		MongoPassword:           c.MongoPassword,
		MongoHost:               c.MongoHost,
		MongoConnectionSettings: c.MongoConnectionSettings,
	})
	if err != nil {
		logger.Fatalf("Error creating mongoclient: %s", err)
	}

	recService := &record.Service{
		RecordsRepo: db.NewRecordsRepo(db.RecordsRepoConfig{
			DBName:         MongoDatabaseName,
			CollectionName: RecordsCollectionName,
			MongoClient:    mongoClient,
			Logger:         logger,
		}),
		Logger: logger,
	}

	return &Dependencies{
		Config:      c,
		Logger:      logger,
		MongoClient: mongoClient,

		KeyValueService: kvs,
		RecordService:   recService,
	}
}
