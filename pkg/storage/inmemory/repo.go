package inmemory

import (
	"context"

	"github.com/dsinecos/go-app/pkg/domain"
	"github.com/dsinecos/go-app/pkg/logger"
)

type InMemoryRepo struct {
	*Store
	Logger *logger.Logger
}

func (r *InMemoryRepo) Put(ctx context.Context, key, value string) domain.Cell {
	r.Lock()
	defer r.Unlock()

	c := domain.Cell{
		Key:   key,
		Value: value,
	}

	r.store[c.Key] = c.Value

	return c
}

func (r *InMemoryRepo) Get(ctx context.Context, key string) (domain.Cell, bool) {
	r.RLock()
	defer r.RUnlock()

	value, ok := r.store[key]

	if !ok {
		return domain.Cell{}, ok
	}

	c := domain.Cell{
		Key:   key,
		Value: value,
	}

	return c, ok
}
