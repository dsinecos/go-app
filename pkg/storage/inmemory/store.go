package inmemory

import "sync"

type Store struct {
	*sync.RWMutex
	store map[string]string
}

func NewStore() *Store {
	return &Store{
		RWMutex: &sync.RWMutex{},
		store:   make(map[string]string),
	}
}
