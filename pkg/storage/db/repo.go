package db

import (
	"context"
	"time"

	"github.com/dsinecos/go-app/pkg/domain"
	"github.com/dsinecos/go-app/pkg/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	RepoName                      string = "RecordsRepo"
	GetRecordsBetweenDateAndCount string = "GetRecordsBetweenDateAndCount"
)

type RecordsRepoConfig struct {
	DBName         string
	CollectionName string
	MongoClient    *mongo.Client
	Logger         *logger.Logger
}
type RecordsRepo struct {
	config     RecordsRepoConfig
	collection *mongo.Collection
	logger     *logger.Logger
}

func NewRecordsRepo(c RecordsRepoConfig) *RecordsRepo {
	return &RecordsRepo{
		config:     c,
		collection: c.MongoClient.Database(c.DBName).Collection(c.CollectionName),
		logger:     c.Logger,
	}
}

type aggregateRecord struct {
	Key        string    `bson:"key,omitempty"`
	CreatedAt  time.Time `bson:"createdAt,omitempty"`
	TotalCount int       `bson:"totalCount,omitempty"`
}

func (r *RecordsRepo) GetRecordsBetweenDateAndCount(ctx context.Context, startDate, endDate time.Time, minCount, maxCount int) ([]domain.AggregateRecord, error) {

	const Op = RepoName + ":" + GetRecordsBetweenDateAndCount

	filterBetweenDates := bson.D{
		{
			Key: "$match",
			Value: bson.D{
				{
					Key:   "createdAt",
					Value: bson.D{{Key: "$gte", Value: startDate}, {Key: "$lte", Value: endDate}},
				},
			},
		},
	}

	sumCounts := bson.D{
		{
			Key: "$project",
			Value: bson.D{
				{
					Key: "totalCount",
					Value: bson.D{
						{
							Key:   "$sum",
							Value: "$counts",
						},
					},
				},
				{
					Key:   "_id",
					Value: 0,
				},
				{
					Key:   "key",
					Value: 1,
				},
				{
					Key:   "createdAt",
					Value: 1,
				},
			},
		},
	}

	filterBetweenTotalCount := bson.D{
		{
			Key: "$match",
			Value: bson.D{
				{
					Key:   "totalCount",
					Value: bson.D{{Key: "$gte", Value: minCount}, {Key: "$lte", Value: maxCount}},
				},
			},
		},
	}

	pipeline := []bson.D{
		filterBetweenDates,
		sumCounts,
		filterBetweenTotalCount,
	}

	// TODO - Handle cursor properly to avoid memory overflow
	recordsCursor, err := r.collection.Aggregate(ctx, pipeline)
	if err != nil {
		// TODO - Update error handling on a case by case basis
		return nil, &domain.Error{
			Code: domain.EINTERNAL,

			Op:  Op,
			Err: err,
		}
	}

	var rec []aggregateRecord
	if err := recordsCursor.All(ctx, &rec); err != nil {
		// TODO - Update error handling on a case by case basis
		return nil, &domain.Error{
			Code: domain.EINTERNAL,

			Op:  Op,
			Err: err,
		}
	}

	var records []domain.AggregateRecord

	for _, v := range rec {
		records = append(records, domain.AggregateRecord{
			Key:        v.Key,
			CreatedAt:  v.CreatedAt,
			TotalCount: v.TotalCount,
		})
	}

	return records, nil
}
