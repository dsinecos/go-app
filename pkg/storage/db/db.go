package db

import (
	"context"
	"fmt"
	"sync"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	MONGO_URI_LAYOUT = "%s://%s:%s@%s/?%s" // MongoScheme://MongoUser:MongoPassword@MongoHost/?MongoConnectionSettings
)

var mongoClient *mongo.Client
var mongoClientError error
var mongoOnce sync.Once

type MongoClientConfig struct {
	MongoScheme             string
	MongoUser               string
	MongoPassword           string
	MongoHost               string
	MongoConnectionSettings string
}

func GetMongoClient(c MongoClientConfig) (*mongo.Client, error) {

	connectionString := fmt.Sprintf(MONGO_URI_LAYOUT, c.MongoScheme, c.MongoUser, c.MongoPassword, c.MongoHost, c.MongoConnectionSettings)

	mongoOnce.Do(func() {
		clientOptions := options.Client().ApplyURI(connectionString)

		client, err := mongo.Connect(context.Background(), clientOptions)

		if err != nil {
			mongoClientError = fmt.Errorf("error connecting to mongodb %w", err)
			mongoClient = nil
			return
		}

		err = client.Ping(context.Background(), nil)
		if err != nil {
			mongoClientError = fmt.Errorf("error pinging to mongodb %w", err)
			mongoClient = nil
			return
		}

		mongoClient = client
	})

	return mongoClient, mongoClientError
}
