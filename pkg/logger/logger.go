package logger

import (
	"fmt"

	"go.elastic.co/ecszap"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	defaultLogFormat = "json"
)

type LogLevel string

const (
	LogLevelFatal LogLevel = "fatal"
	LogLevelWarn  LogLevel = "warn"
	LogLevelPanic LogLevel = "panic"
	LogLevelError LogLevel = "error"
	LogLevelInfo  LogLevel = "info"
	LogLevelDebug LogLevel = "debug"
)

type Logger struct {
	*zap.SugaredLogger
}

func New(logLevel LogLevel) (*Logger, error) {

	config := zap.NewProductionConfig()

	config.Encoding = defaultLogFormat

	var level zapcore.Level
	err := level.UnmarshalText([]byte(logLevel))
	if err != nil {
		return nil, fmt.Errorf("error configuring logger: %w", err)
	}
	config.Level = zap.NewAtomicLevelAt(level)

	// Transform field keys to make them compatible with Elastic Common Schema
	config.EncoderConfig = ecszap.ECSCompatibleEncoderConfig(config.EncoderConfig)

	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("error building logger: %w", err)
	}

	return &Logger{SugaredLogger: logger.Sugar()}, nil
}
